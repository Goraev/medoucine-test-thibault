<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function therapists()
    {
        return $this->hasMany(Therapist::class);
    }

    public function practices()
    {
        return $this->belongsToMany(Practice::class);
    }
}
