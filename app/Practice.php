<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Practice extends Model
{
    public $timestamps = false;

    protected $hidden = ['pivot', 'timestamp'];

    public function therapists()
    {
        return $this->belongsToMany(Therapist::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }
}

