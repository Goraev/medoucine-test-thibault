<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Therapist extends Model
{
    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function practices()
    {
        return $this->belongsToMany(Practice::class);
    }

    public function getPracticesLabelsAttribute()
    {
        return $this->practices->pluck('name')->sort()->implode(', ');
    }
}
