<?php

use App\City;
use App\Practice;
use App\Therapist;
use App\Http\Resources\CityResource;
use App\Http\Resources\PracticeResource;
use App\Http\Resources\TherapistResource;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/cities', function () {
    return CityResource::collection(City::all());
});

Route::get('/therapists', function (Request $request) {
    $transaction = Therapist::query();

    if ($request->has('city_id')) {
        $transaction->where(['city_id' => $request->query('city_id')]);
    }

    $collection = $transaction->get();

    if ($request->has('practice_id')) {
        return TherapistResource::collection($collection->filter(function ($therapist) use ($request) {
            return in_array($request->query('practice_id'), $therapist->practices->pluck('id')->toArray());
        }));
    }

    return TherapistResource::collection($collection);
});
