## Fullstack developper test

Here a test for Medoucine. You can find the original repo [there](https://github.com/medoucine/fullstack-recruitment-test)

## Installation
1. Clone the projet `git clone git@gitlab.com:Goraev/medoucine-test-thibault.git`
2. Dive in the project `cd medoucine-test-thibault`
3. Run the build script `sh bin/build.sh`
4. Visit the result of my test at [http://127.0.0.1:8000](http://127.0.0.1:8000)

## Note
- Application is already tested when you launch the build script meanwhile you can launch them separately.

- PHPUnit: `php vendor/phpunit/phpunit/phpunit`
- Jest: `cross-env NODE_ENV=test jest`

## Prerequisites
- PHP7.x
- Node >=10
- Yarn
- PHP composer
