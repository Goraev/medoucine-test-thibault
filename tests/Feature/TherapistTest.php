<?php

namespace Tests\Feature;

use Tests\TestCase;

class TherapistTest extends TestCase
{
    public function testTherapist()
    {
        $response = $this->json('GET', 'api/therapists');
        $response->assertStatus(200);
        // We know the assert count because, we created the json_test.file
        $response->assertJsonCount(9, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'name',
                    'city',
                    'practices',
                    'description'
                ]
            ]
        ]);
    }

    /**
     * @dataProvider queryStringProvider
     */
    public function testTherapistWithQueryString($queryString, $expectedCount)
    {
        $response = $this->json('GET', 'api/therapists'.$queryString);
        $response->assertStatus(200);
        $response->assertJsonCount($expectedCount, 'data');
    }

    public function queryStringProvider()
    {
        return [
            ['?city_id=1', 3],
            ['?city_id=2', 3],
            ['?city_id=3', 3],
            ['?practice_id=1', 7],
            ['?practice_id=2', 5],
            ['?practice_id=3', 5],
            ['?practice_id=4', 3],
            ['?practice_id=5', 3],
            ['?practice_id=6', 2],
            ['?practice_id=7', 2],
            ['?practice_id=8', 1],
            // Count few association (not all)
            ['?city_id=1&practice_id=1', 2],
            ['?city_id=2&practice_id=4', 1],
            ['?city_id=3&practice_id=6', 0],
        ];
    }
}