<?php

namespace Tests\Feature;

use Tests\TestCase;

class CityTest extends TestCase
{
    public function testCity()
    {
        $response = $this->json('GET', 'api/cities');

        $response->assertStatus(200);
        // We know the assert count because, we created the json_test.file
        $response->assertJsonCount(3, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'practices' => [
                        '*' => [
                            'id',
                            'name',
                        ]
                    ]
                ]
            ]
        ]);
    }
}