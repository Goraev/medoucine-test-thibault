#!/bin/bash
set -e

currentDIR=`pwd`
project_dir="$(dirname "$dir")"

function run() {
    install
    test
    build
    serve
}

function install() {
    touch database/database.sqlite
    touch database/database_test.sqlite
    composer install
    npm install
    npm run development
}

function test() {
    configure_dot_env_test
    php artisan migrate:fresh --env=testing && php artisan db:seed --env=testing
    php vendor/phpunit/phpunit/phpunit
    yarn test:unit
}

function build() {
    configure_dot_env
    php artisan migrate:fresh && php artisan db:seed
}

function serve() {
    php artisan serve
}

function configure_dot_env_test() {
    sed "s|PATH_TO_PROJECT|$(pwd)|g" ".env.testing" > .env.testing.new
    mv .env.testing .env.testing.old
    mv .env.testing.new .env.testing
    rm -f .env.testing.old
}

function configure_dot_env() {
    sed "s|PATH_TO_PROJECT|$(pwd)|g" ".env" > .env.new
    mv .env .env.old
    mv .env.new .env
    rm -f .env.old
}

run