export let cities = [
    {
        "id": 1,
        "name": "Lyon",
        "practices": [
            {
                "id": 2,
                "name": "Acupuncture"
            },
            {
                "id": 3,
                "name": "Analyse transgénérationnelle"
            },
        ]
    },
    {
        "id": 2,
        "name": "Paris",
        "practices": [
            {
                "id": 1,
                "name": "Access Bars"
            },
            {
                "id": 3,
                "name": "Analyse transactionnelle"
            },
            {
                "id": 4,
                "name": "Analyse transgénérationnelle"
            },
            {
                "id": 5,
                "name": "Aromathérapie"
            }
        ]
    },
    {
        "id": 3,
        "name": "Toulouse",
        "practices": [
            {
                "id": 2,
                "name": "Acupuncture"
            },
            {
                "id": 8,
                "name": "Auriculothérapie"
            },
        ]
    }
]