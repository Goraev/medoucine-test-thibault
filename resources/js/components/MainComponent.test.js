import {createLocalVue, shallowMount} from '@vue/test-utils'
import flushPromises from "flush-promises";
import Component from './MainComponent.vue'
import {cities} from './MainComponentTestExports.js'

jest.mock("axios", () => ({
    get: () => Promise.resolve({
        data: {
            data: [
                {
                    "name": "Prof. Trudie Hickle DDS",
                    "city": "Lyon",
                    "practices": "Analyse transgénérationnelle, Auriculothérapie, Chiropraxie",
                    "description": "Sed corporis in libero iste consequuntur eius expedita eveniet."
                },
                {
                    "name": "Jeremie Brakus",
                    "city": "Lyon",
                    "practices": "Analyse transgénérationnelle, Atelier, Gestion des émotions",
                    "description": "Quas nisi quidem libero velit tempore eos eius."
                },
                {
                    "name": "Andrew Anderson",
                    "city": "Lyon",
                    "practices": "Analyse transgénérationnelle, Auriculothérapie, Coaching",
                    "description": "Accusamus aut aut deserunt atque repellendus."
                }
            ]
        }
    })
}));

const chooseCity = 'Choisir une ville'
const choosePractice = 'Choisir un thérapeute'

const factory = (values = {}) => {
    const localVue = createLocalVue()
    return shallowMount(Component, {
        localVue,
        propsData: {
            cities
        },
        data() {
            return {
                cityPlaceHolder: chooseCity,
                practicePlaceHolder: choosePractice,
                practices: [],
                therapists: [],
                city: null
            }
        }
    })
}

describe('Component', () => {
    it('Test choose city placeholder', async () => {
        const wrapper = factory();

        expect(wrapper.findComponent({ref: 'chooseCity'}).text()).toEqual(chooseCity)
        expect(wrapper.findComponent({ref: 'choosePractice'}).text()).toEqual(choosePractice)

        await wrapper.find('.city-link-1').trigger('click')
        expect(wrapper.findComponent({ref: 'chooseCity'}).text()).toEqual(cities[0].name)

        await wrapper.vm.chooseCity(cities[1]);
        expect(wrapper.findComponent({ref: 'chooseCity'}).text()).toEqual(cities[1].name)
    })

    it('Test practice list depend on city', async () => {
        const wrapper = factory();

        await wrapper.find('.city-link-2').trigger('click')
        expect(wrapper.findComponent({ref: 'choosePractice'}).element.nextElementSibling.childElementCount)
            .toEqual(cities[1].practices.length)
    })

    it('Test results when clicking on Go ! ', async () => {
        const wrapper = factory();

        await wrapper.find('div.choose-city > a:nth-of-type(2)').trigger('click')
        await wrapper.find('div.choose-practice > a:first-of-type').trigger('click')
        await wrapper.findComponent({ref: 'getTherapists'}).trigger('click')

        await flushPromises()
        expect(wrapper.vm.therapists.length).toEqual(3)
        expect(wrapper.vm.therapists[2].name).toEqual('Andrew Anderson')

        expect(wrapper.findAll('div.card-body').exists()).toBe(true)
    })
})
