<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticeTherapistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_therapist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('practice_id');
            $table->foreign('practice_id')
                ->references('id')
                ->on('practices')
                ->onDelete('cascade');
            $table->integer('therapist_id');
            $table->foreign('therapist_id')
                ->references('id')
                ->on('therapists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practices_therapists');
    }
}
