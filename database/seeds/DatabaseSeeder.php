<?php

use App\City;
use App\Therapist;
use App\Practice;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $seed = collect(json_decode(file_get_contents(database_path(env('SEED_FILE_NAME'))), true));
        // Seed practices table
        $seed->groupBy('practices')->keys()->sort()->values()->each(function ($practice) {
            DB::table('practices')->insert([
                'name' => $practice,
            ]);
        });

        // Seed cities table
        $seed->groupBy('city')->keys()->sort()->values()->each(function ($city) {
            DB::table('cities')->insert([
                'name' => $city,
            ]);
        });

        // Seed therapists table
        $seed->each(function ($therapist) {
            DB::table('therapists')->insert([
                'name' => $therapist['name'],
                'description' => $therapist['description'],
            ]);
        });

        // Add city and practices to all therapists
        Therapist::all()->each(function ($therapist, $key) use ($seed) {
            $city = City::where('name', $seed[$key]['city'])->first();
            $therapist->city_id = $city->id;

            collect($seed[$key]['practices'])->each(function ($practise) use ($therapist) {
                $therapist->practices()->attach(Practice::where('name', $practise)->first());
            });

            $therapist->save();
        });

        // Fill the pivot city-practice for easy exposition
        $seed->each(function ($therapist) {
            $city = City::where('name', $therapist['city'])->first();

            collect($therapist['practices'])->each(function ($practise) use ($city) {
                $city->practices->pluck('id');
                $practise = Practice::where('name', $practise)->first();

                if (!in_array($practise->id, $city->practices->pluck('id')->toArray())) {
                    $city->practices()->attach($practise);
                }
            });

            $city->save();
        });
    }
}
